<?php
namespace Infosophie\Sass;

use ScssPhp\ScssPhp\Compiler;

class Sass {

    private static $folders = [];

    public static function addPath($path, $callback) {
        if (is_readable($path)) {
            echo "readable absolute";
            self::compileDueFiles($path, $callback);
            return true;
        }
        if (is_readable(__DIR__ . "/" . $path)) {
            echo "readable relative";
            self::compileDueFiles(__DIR__ . "/" . $path, $callback);
            return true;
        }
        if (is_callable($callback)) {
            // Autoloading will be invoked to load the class "ClassName" if it's not
            // yet defined, and PHP will check that the class has a method
            // "someStaticMethod". Note that is_callable() will NOT verify that the
            // method can safely be executed in static context.
            echo "callback not callable";
            return call_user_func($callback, ['class' => 'Infosophie\Sass\Sass', 'function' => 'addPath', 'error' => 'Path is not readable']);
        }
        trigger_error("Error handler passed to addPath() is not a callable callback function!", E_USER_ERROR);
    }

    private function compileDueFiles($path, $callback) {
        $dir = opendir($path);
        while ($item = readdir($dir)) {
            echo "\n<br>File: " . $item;
            $info = pathinfo($path . "/" . $item);
            $ext = $info['extension'];
            echo " - EXT: " .$ext;
            if ($ext == 'scss') {
                " - ext matching ";
                $cssfile = realpath($info['dirname']). "/" . $info['filename'] . ".css";
                $sassfile = realpath($info['dirname']. "/" . $info['filename'] . ".scss");
                echo " - CSS: " . $cssfile;
                echo " - SCSS: " . $scssfile;
                if (is_readable($sassfile)) {
                    if (!file_exists($cssfile) || filemtime($cssfile) > filemtime($sassfile)) {
                        echo "compile it!";
                        self::compile($sassfile, $cssfile, $callback);
                    }
                }
            }
        }
        return $duefiles;
    }

    private function compile($sassfile, $cssfile, $callback) {

        $compiler = new Compiler();
echo "\n<br>CSSFILE: " . $cssfile;
echo "\n<br>";
        if (is_readable($sassfile)) {
            $sass = file_get_contents($sassfile);
            $css = $compiler->compileString($sass)->getCss();
            try {
                file_put_contents($cssfile, $css);
            } catch (Exception $e) {
                call_user_func($callback, ['e' => $e, 'class' => 'Infosophie\Sass\Sass', 'function' => 'compile', 'target' => $cssfile, 'error' => 'CSS file is not writable']);
            }
        }

    }

}
