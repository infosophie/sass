
## Infosophie::Sass

This is a lightweight and simple interface to use the package 'scssphp/scssphp' in your PHP project. For now there is one simple function:
Searching *.scss files in a given folder (local path) and eventually compile them and create (overwrite) respective *.css files. Compilation only takes place if one of the two conditions are fullfilled:
1. there is a *.scss file in the folder, but no corresponding *.css file
2. the *.css file is newer (i.e. was changed since the last compilation) than the corresponding *.scss file
The conditional compliation allows you to call Infosophy::Sass every time your script runs without performance loss.

## Installation
### Installation with composer (recommended)
If you're familiar with Composer (package management tool for PHP), just add the following statement to your composer.json file:
~~~json
{
    "require": {
        "infosophie/sass": "^0.1.2"
    }
}
~~~
#### Version hints
Version 0.1.2 is the first working version. Feel free to go for a later version or to use "dev-main" to always get the latest stable version.
#### How to edit an existing composer.json file
If your composer.json file already contains other statements, you might leave off the first opening and last closing bracket. If your composer.json file already contains other "require" items, you might just add the respective line ("infosophie/sass": "^0.1.2") to the existing require block.
A more complex composer.json file might look anything like this:
~~~json
{
    "name": "your project name",
    "description": "Most valuable functions!",
    "license": "BSD-3-Clause",
    "keywords": [
        "project",
        "function"
    ],
    "homepage": "https://your-web.com",
    "require": {
        "php": "^7.3 || ~8.0.0 || ~8.1.0",
        "infosophie/sass": "^0.1.2",
        "ext-intl": "*"
    }
}
~~~
From this example you understand, how the crucial row ("infosophie/sass": "^0.1.2") has to be included in your composer.json.

To learn more about composer and it's usage, pls. refer to https://getcomposer.org/

## Manual installation

If you don't use composer, you may install Infosophie::Sass also via FTP or SFTP on your webhost or directly on your local machine.
### 1. Download required files
You may download the whole project from GitLab (https://gitlab.com/infosophie/sass/) or directly from https://gitlab.com/infosophie/sass/-/archive/main/sass-main.zip
### 2. Copy the files to your webroot
Unzip the zip package to your web project folder or copy the files from the GitLab repository to a subfolder in your web project folder. You're free to select any folder to install the package, but it's recommended to use an appropriate subfolder in your project folder. Let's say you use the subfolder "infosophie_sass", then you should find the following file at the given path:

**...<your project root folder>/infosophie_sass/src/Sass.php**

### 3. Install the required packages
As Infosophie::Sass uses an other package, you also have to install this package. It's "scssphp/scssphp" and you find it at https://scssphp.github.io/scssphp/

Download the latest version and unpack the files to another subfolder in your project (e.g. <your project root folder>/scssphp/).

## Usage

First we have to include the php scripts from Infosophie::Saas as well as from the required packages. It depend's on our installation method, how to handle this:

### when installed with Composer, do this:

Call Composer's autoload routine, i.e. include/require the file <your webroot>/vendor/autoload.php and use the Class Infosophy\Sass\Sass.

Example:
~~~php
<?php
namespace Yournamespace;
require "vendor/autoload.php";
~~~

### when installad manually, do this:

If you installed the packages manually, you also have to include them manually. The "scssphp" package provides a single php script to include all the required files. Infosophie::Saas has only one single file to include. These is the code to do this:
~~~php
<?php
namespace Yournamespace;
require "scssphp/scss.inc.php";
require "infosophy_sass/src/Sass.php";
~~~
The example assumes you installed the files in the folders as described in the Installation chapter!

### continue here for both methods:

When you successfully included/required the files, you have to import the classes to be used. There is only one: the Infosophie\Saas\Saas class. The classes from package "scssphp/scssphp" will then be imported recursively by Infosophie::Saas!

Example:
~~~php
use Infosophie\Saas\Saas;
~~~

Now we're ready to use Infosophy::Saas. To use SASS-Files for your project, i.e. compile them automatically to respective CSS files, just call the static function "addFolder()" once for each folder containing SASS files:

Example:
~~~php
Saas::addFolder('path/to/my/styles');
~~~

where path/to/my/styles is the path to the folder that contains your SASS files, relative to the folder of your script. You also may give an absolute path here.

Let's say the absolute path of your web root (where your php script resides) is /hosting/youraccount/public_html/, and your SASS files are located in /hosting/youraccount/public_html/styles/ then you may use one of the two following examples:
~~~php
Saas::addFolder('styles');
~~~
~~~php
Saas::addFolder('/hosting/youraccount/public_html/styles');
~~~

Infosophie::Saas will search this folder (but **not** it's subfolders!) for files ending in *.scss and compile them to *.css files with the same name. The *.css files will be stored in the same folder and existing *.css files will be overwritten with no mercy!

The compilation and creation of *.css files will only be done if the respective *.scss file was changed recently, i.e. is newer than the corresponding *.css file or if no corresponding *.css file exists in the folder! This prevents your script from needlessly compile the same file over and over again!

The following code gives you an example of how your entire script should look like after all these steps (i.e. all the steps above combined).

### With Composer:
~~~php
<?php

namespace Yournamespace;

require "vendor/autoload.php";

use Infosophie\Saas\Saas;

Saas::addFolder('styles');

/* your code... */

?>
~~~
### With manual installation:
~~~php
<?php

namespace Yournamespace;

require "scssphp/scss.inc.php";
require "infosophy_sass/src/Sass.php";

use Infosophie\Saas\Saas;

Saas::addFolder('styles');

/* your code... */

?>
~~~

## Support
There is no support for this project. The only source for information on this project is the git repository at GitLab (https://gitlab.com/infosophie/sass). Please use the links provided in this document to get more information or search the web for relevant keywords like 'sass', 'composer php', 'packagist', 'git', 'repository' etc. and refer to the relevant pages in the PHP documentation if you don't understand the mentioned steps, especially:\
https://www.php.net/manual/en/function.include.php\
https://www.php.net/manual/en/language.oop5.php
https://www.php.net/manual/en/language.namespaces.php\
https://www.php.net/manual/en/language.namespaces.importing.php\

If you use this package commercially (which you are welcome to do!) and/or if you don't feel like looking for all the answers to your questions, you're welcome to use my paid services to solve your problems and answer your questions! Please contact me at mail@cgrauer.de

## Roadmap
There are currently no plans for updates or new features.

## Contributing
If you feel like contributing to this project or if you have ideas for additional features, feel free to push code and add a merge request to the repository at GitLab (https://gitlab.com/infosophie/sass/-/merge_requests/new).

If you find bugs, please use the GitLab issue tracker to report them (https://gitlab.com/infosophie/sass/-/issues/new)

I appreciate any contribution. Feel free to conatact me at mail@cgrauer.de!

## Authors and acknowledgment
Author: Christian Grauer (mail@cgrauer.de). I'm an independent software developer from Germany, working on my own since 1998, focussed on web development, consulting and it trainings.

Thanks to Leaf Corcoran (https://scssphp.github.io/scssphp/) for writing and providing the scssphp/scssphp package!

## License
This project is published under the GNU General Public License Version 3.0 or later (https://www.gnu.org/licenses/gpl-3.0-standalone.html)

